// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SkyGameModeBase.generated.h"

class APawnPlayerShip;
class APlayerControllerBase;

UCLASS()
class SKYCIRCLE_API ASkyGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

private:
	APawnPlayerShip* PlayerShip;
	APlayerControllerBase* PlayerControllerRef;


	void HandleGameStart();
	//void HandleGameOver(bool PlayerWon);

public:

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Game Loop")
	int32 StartDelay = 5;
	UFUNCTION(BlueprintImplementableEvent)
	void GameStart();
	//UFUNCTION(BlueprintImplementableEvent)
	//void GameOver(bool PlayerWon);

};
